package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController //将返回值转化为JSON
@CrossOrigin    //加到controller或者方法中 所有的请求都能访问
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * URL: /addUser
     * method: post
     * 参数: form表单传参  name/age/sex
     * 返回值: 返回成功的字符信息
     */
    @PostMapping("/addUser")
    public String addUser(User user){

        userService.addUser(user);
        return "新增成功!!";
    }

    /**
     * 查询User表的全部数据
     * 请求路径: http://localhost:8090/getUser
     * 参数:     不需要参数
     * 返回值:  List<User>
     */
    @GetMapping("/getUser")
    public List<User> getUser(){

        List<User> userList = userService.getUser();
        return userList;
    }

    /**
     * URL: http://localhost:8090/servlet?id=100&name=tomcat
     * @return
     * request: 用户提交请求时的请求头信息.
     * response: 服务器响应用户的数据封装的对象
     */
   /* @RequestMapping("/servlet")
    public String servlet(HttpServletRequest request, HttpServletResponse response){
        //servlet动态接收参数 所有的数据都是String数据类型
        String id = request.getParameter("id");
        int idInt = Integer.parseInt(id);
        String name = request.getParameter("name");
        //SpringMVC将上述的操作封装
        return null;
    }*/
    /*@RequestMapping("/servlet")
    public String servlet(Integer id,String name){

        return null;
    }*/

}
