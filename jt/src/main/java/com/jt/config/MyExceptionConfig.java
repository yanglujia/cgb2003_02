package com.jt.config;

import com.jt.vo.SysResult;
import org.apache.ibatis.session.SqlSessionException;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//问题: 全局异常的处理 应该拦截哪一层的代码?? mapper,service,controller????
//@ControllerAdvice + @ResponseBody
@RestControllerAdvice //全局异常处理 返回值为JSON
public class MyExceptionConfig {

    //问题2: 什么时候调用? 有异常的时候调用
    //数组的写法
    //@ExceptionHandler({RuntimeException.class, SqlSessionException.class})
    @ExceptionHandler({RuntimeException.class})
    public Object handler(Exception exception){
        //将报错信息控制台打印
        exception.printStackTrace();
        return SysResult.fail();
    }
}
