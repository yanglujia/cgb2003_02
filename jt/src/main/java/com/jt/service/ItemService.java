package com.jt.service;

import com.jt.pojo.Item;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;

public interface ItemService {
    PageResult findItemByPage(PageResult pageResult);

    void updateItem(Item item);

    void saveItem(ItemVO itemVO);
}
