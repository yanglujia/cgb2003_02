package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;
//@Mapper //为接口创建了反射机制一个对象
//MP规则: BaseMapper<T> T MP要操作的表是谁? T必须引入对象
public interface UserMapper extends BaseMapper<User> {

    //查询全部用户信息 访问修饰符 返回值类型 方法名(参数...)
    List<User> findAll();

    @Insert("insert into demo_user(id,name,age,sex) " +
            "value (null,#{name},#{age},#{sex})")
    void insertUser(User user);
    @Update("update demo_user set name = #{newName} where name=#{oldName}")
    void updateByName(String oldName, String newName);
    //Mybatis中如果传递的参数只有一个,则名称任意 一般不用.
    @Delete("delete from demo_user where name=#{name}")
    void deleteByName(String name);
}
