package com.jt.test;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class TestMP2 {

    @Autowired
    private UserMapper userMapper;

    /**
     * 将ID=229的用户名称 改为六一儿童节
     */
    @Test
    public void updateUser(){
        User user =  new User();
        user.setId(229).setName("六一儿童节");
        //set name="xxx" where id = 229
        userMapper.updateById(user);
    }

    /**
     * 更新操作2
     *      将name="六一儿童节" 改为"端午节"
     *  参数说明:
     *         1.实体对象  封装修改后的数据 set结构
     *         2.UpdateWrapper 修改的条件构造器
     *  Sql: update demo_user set name="端午节" where name="61"
     */
    @Test
    public void updateUser2(){
        User user = new User();
        user.setName("端午节");
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("name", "六一儿童节");
        userMapper.update(user,updateWrapper);
    }
}
