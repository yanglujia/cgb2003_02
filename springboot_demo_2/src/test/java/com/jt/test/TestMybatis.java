package com.jt.test;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

//SpringBoot为了简化程序的测试,研发了一个注解@SpringBootTest
//该注解只能在测试包中使用.
//作用: 当程序执行@Test测试方法时,则会动态的启动整个Spring容器,之后通过 @Autowired
//从容器中获取获取实例化对象,之后完成业务的测试.
@SpringBootTest
public class TestMybatis {
    // sprign容器 <userMapper,代理对象>
    // 面向接口编程 扩展性好
    @Autowired
    private UserMapper userMapper;//JDK动态代理

    //报错说明:com.jt.mapper.UserMapper.findAll  不匹配!!!!
    //关于测试类代码说明: 要求: public   返回值void  方法名称不能叫test
    @Test
    public void test01(){
        System.out.println(userMapper.getClass());
        List<User> userList = userMapper.findAll();   //接口的方法  数据库只能识别Sql语句
        System.out.println(userList);
    }

    @Test
    public void insert(){
        User user = new User();
        user.setName("星期五").setAge(18).setSex("男");
        userMapper.insertUser(user);
        System.out.println("新增用户成功");
    }

    //根据name="星期五",将name="星期六"
    @Test
    public void update(){
        String oldName = "星期五";
        String newName = "星期六";
        userMapper.updateByName(oldName,newName);
        System.out.println("更新用户成功");
    }

    //删除用户信息 根据name属性删除数据
    @Test
    public void delete(){
        String name = "星期六";
        userMapper.deleteByName(name);
        System.out.println("删除用户成功");
    }

    //查询 用户的全部记录 面向对象的方式操作数据库.
    //只能用于单表查询
    @Test
    public void testFind(){
        //暂时不需要任何where条件  查询的是全部记录.
        List<User> userList = userMapper.selectList(null);
        System.out.println(userList);
    }

    /**
     * 利用MP实现用户数据入库
     */
    @Test
    public void testInsert(){
        User user = new User();
        user.setName("嫦娥").setAge(20).setSex("女");
        userMapper.insert(user);
    }
}
